#include <mega32.h>
#include <delay.h>
#include <math.h>
#include <stdlib.h>

#include <spi.h>

#define uchar unsigned char
#define schar signed char
#define uint unsigned int

// ��������� ���������
const uchar ADMUX_INIT = 0b00000000;
const uchar ADP_RUN = 6;
const uchar matrix_rows = 4, matrix_columns = 4;

     // ��������� ������������� ������ �����-������
void init_ports();

     // ���������� �������
void init_threshholds( uchar *th_basic, uchar *th_accurate );

     // ���������� ��������
void init_delays( uint *line_set_delay, uint *engine_move_delay );

     // ��������� ������ � row_number � ������� mat
void read_row( uint mat[][matrix_columns], uchar row_number, uint line_set_delay );

     // ������ ������ � ����������� ����
uint read_adp( uchar pin_number );

     // ������ ���������� ��������� "engine" ��������� � ����������� "direction" �� "step_size"
void increment_engine( schar *engine, int direction, uchar step_size );

     // ������������� ������� ����� ������ ��� ���������� � ������������ � �� ����������� �����������
void move_engine( schar engine_vert, schar engine_horiz, uint engine_move_delay );

      // �������� �������� ����� ��� �������� ����������� ��������� ���������
uchar engine_command( schar engine );

/*
������������ ������������� ��������.
��� ��������� ������ �� +5�:
PORTD0 - ������������� �������� ������������� ������ � ������������� �����������
PORTD1 - ������������� �������� ������������� ������ � ������������� �����������
������� can_move_vert ���������� 0, ���� ��������� � ������ �� �������� ����������� �������� ������������� ���������
*/
uchar can_move_vert( int vert_mismatch );

/*
�������� �������� ��������������� �� SPI
*/
void send_mismatch_value( uchar mismatch );

/*
����� �������
��� ��������� ������ �� +5�:
PORTD7 - ���������� ����� �������
PORTD6 - ���������� ����������� ����� (� ������ �������)
PORTD0, PORTD1 - �������� ��������������� ������ � ��� ������� (� ������ �������)
PORTD2, PORTD3 - �������� ������������� ������ � ��� ������� (� ������ �������)
*/
uchar process_debug_command( schar *engine_vert, schar *engine_horiz, uchar *th_basic, uchar *th_accurate, uint *line_set_delay, uint *engine_move_delay );

void main()
{
   uchar row;
   // ����� �������� ��� ������� "���������" �������
   uint q1, q2, q3, q4;
   // �������� ��� �������� ���������� ���������
   uint line_set_delay, engine_move_delay;
   // ������ ��� ���������������
   uchar th_basic, th_accurate;
   // �������� �������
   uint mat[matrix_rows][matrix_columns];
   // ���������� ��������� ������������� � ��������������� ���������
   schar engine_vert, engine_horiz;
   // ���������������
   int vert_mismatch_basic, vert_mismatch_accurate, horiz_mismatch_basic, horiz_mismatch_accurate;

   // �������������� ��������� ��������� ����������
   engine_vert = 0;
   engine_horiz = 0;
   
   init_ports();
   init_threshholds( &th_basic, &th_accurate );
   init_delays( &line_set_delay, &engine_move_delay );

   while( 1 )
   {     
      if( process_debug_command( &engine_vert, &engine_horiz, &th_basic, &th_accurate, &line_set_delay, &engine_move_delay ) )
         continue;
         
      // ��������� ��������� �������� �������
      for( row = 0;  row < matrix_rows; row++ ) 
         read_row( mat, row, line_set_delay );
      PORTB = 0x0;
      
      // ������� ����� ��������� �������
      q1 = mat[0][0] + mat[0][1] + mat[1][0] + mat[1][1];
      q2 = mat[0][1] + mat[0][3] + mat[1][2] + mat[1][3];
      q3 = mat[2][0] + mat[2][1] + mat[3][0] + mat[3][1];
      q4 = mat[2][2] + mat[2][3] + mat[3][2] + mat[3][3];

      // ��������������� �� ���������
      vert_mismatch_basic = ( q1 + q2 ) - ( q3 + q4 );
      vert_mismatch_accurate = ( mat[1][1] + mat[1][2] ) - ( mat[2][1] + mat[2][2] );

      // ������ ����� ����������� ��������� ������������� ���������
      if( abs( vert_mismatch_basic ) > th_basic )
      {
         if( can_move_vert( vert_mismatch_basic ) )
            increment_engine( &engine_vert, vert_mismatch_basic, 2 );
      }
      else if( abs( vert_mismatch_accurate ) > th_accurate )
      {
         if( can_move_vert( vert_mismatch_accurate ) )
            increment_engine( &engine_vert, vert_mismatch_accurate, 1 );
      }
      
      // ��������������� �� �����������
      horiz_mismatch_basic = ( q1 + q3 ) - ( q2 + q4 );
      horiz_mismatch_accurate = ( mat[1][1] + mat[2][1] ) - ( mat[1][2] + mat[2][2] );
      
      send_mismatch_value( horiz_mismatch_basic );

      // ������ ����� ����������� ��������� ��������������� ���������
      if( abs( horiz_mismatch_basic ) > th_basic )
         increment_engine( &engine_horiz, horiz_mismatch_basic, 2 );
      else if( abs( horiz_mismatch_accurate ) > th_accurate )
         increment_engine( &engine_horiz, horiz_mismatch_accurate, 1 );

      // ������ �������� ������ ������ � ������������ � �������� ����������� ����������� ����������
      move_engine( engine_vert, engine_horiz, engine_move_delay );
   }
}

void init_ports()
{
   // ������������ PORTB, PORTC ��� ������
   DDRB = 0xFF;
   DDRC = 0xFF;   
   DDRD = 0xFF;
   
   // ��������� ���.
   ADCSR = 0b10000101;

   // ��������� SPI
   spi_init( true, false, SPI_MODE_0_gc, false, 0, PORTB );
}

uchar get_th_value( uint adp_value, uchar default_value )
{
   if( adp_value == 0 )
      return default_value;
      
   return adp_value;
}

void init_threshholds( uchar *th_basic, uchar *th_accurate )
{
   *th_basic = get_th_value( read_adp( 4 ), 200 );
   *th_accurate = get_th_value( read_adp( 5 ), 50 );
}

uint get_delay_value( uint adp_value, uint default_value )
{
   if( adp_value == 0 )
      return default_value;
      
   return adp_value;
}

void init_delays( uint *line_set_delay, uint *engine_move_delay )
{
   *line_set_delay = get_delay_value( read_adp( 6 ), 10 );
   *engine_move_delay = get_delay_value( read_adp( 7 ), 50 );
}

void increment_engine( schar *engine, int direction, uchar step_size )
{
   if( direction > 0 )
   {
      *engine += step_size;
      if( *engine > 7 )
         *engine -= 8;
   }
   else
   {
      *engine -= step_size;
      if( *engine < 0 )
         *engine += 8;
   }
}

uchar engine_command( schar engine )
{
   // � ����������� �� ����������� ��������� ��������� ���������� ����� ��� ��� ����� ������
   switch( engine )
   {
      case 0: return 0b00000001;
      case 1: return 0b00000011;
      case 2: return 0b00000010;
      case 3: return 0b00000110;
      case 4: return 0b00000100;
      case 5: return 0b00001100;
      case 6: return 0b00001000;
      case 7: return 0b00001001;
   }
   
   return 0b00000000;
}

// ��� ������� ������������ ��������� ���������� ���������� ��������������� �������� PORTC
void move_engine( schar engine_vert, schar engine_horiz, uint engine_move_delay )
{
   // ������������� ���� ����� ������ ��� ���������� ��������� ������������� � ��������������� ����������
   PORTC = engine_command( engine_vert ) | ( engine_command( engine_horiz ) << 4 );
   delay_ms( engine_move_delay );
}

void read_row( uint mat[][matrix_columns], uchar row_number, uint line_set_delay )
{
   uchar i;
   
   // ���������� ������ row_number
   PORTB = 0b00000001 << row_number;
   
   delay_ms( line_set_delay );
   
   // ��������� �������� 
   for( i = 0; i < matrix_columns; i++ )
      mat[row_number][i] = read_adp( i );
}

uint read_adp( uchar pin_number )
{
   // ������ ���� ��� ������ ���
   ADMUX = ADMUX_INIT | pin_number;
   
   // ��������� ��������������
   ADCSR |= ( 1 << ADP_RUN );
   
   // ������� �������� ��������������
   while( ADCSR & ( 1 << ADP_RUN ) ) {}
   return ADCW;
}

uchar can_move_vert( int vert_mismatch )
{  
   // ���� ����� ��������� � ������������� �����������, 
   // �� ��������� � ������� �� �������������� �����������
   if( vert_mismatch > 0 && ( PIND & ( 1 << 0 ) ) )
      return 0;
      
   // ���� ����� ��������� � ������������� �����������, 
   // �� ��������� � ������� �� �������������� �����������
   if( vert_mismatch < 0 && ( PIND & ( 1 << 1 ) ) )
      return 0;
      
   PORTD = 0x0;
   return 1;
}

void send_mismatch_value( uchar mismatch )
{
   spi_master_ss_low( PORTB )
   spi( mismatch );
   spi_master_ss_high( PORTB )
   return;
}

uchar process_debug_command( schar *engine_vert, schar *engine_horiz, uchar *th_basic, uchar *th_accurate, uint *line_set_delay, uint *engine_move_delay )
{
   uchar step_size;   
 
   // ���� �� � ������ ������� - �������.
   if( !( PIND & ( 1 << 7 ) ) )
      return 0;
   
   init_threshholds( th_basic, th_accurate );
   init_delays( line_set_delay, engine_move_delay );
   
   // �������� ������ ���� � ����������� �� ����� �� PORTD6
   step_size = 2;
   if( PIND & ( 1 << 6 ) )
      step_size = 1;
      
   if( PIND & ( 1 << 0 ) )
      increment_engine( engine_horiz, 1, step_size );
   if( PIND & ( 1 << 1 ) )
      increment_engine( engine_horiz, -1, step_size );
   
   if( PIND & ( 1 << 2 ) )
      increment_engine( engine_vert, 1, step_size );
   if( PIND & ( 1 << 3 ) )
      increment_engine( engine_vert, -1, step_size );
   
   PORTD = 0x0;
   move_engine( *engine_vert, *engine_horiz, *engine_move_delay );
   
   return 1;
}
